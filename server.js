var routes = require('./routes')
var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({ type: 'application/vnd.api+json' }))
app.set('port', 1337);

http.createServer(app).listen(app.get('port'),function(){
    console.log('Server Listening on Port : ' + app.get('port'));
});

app.post('/subscription',     routes.setSubscription);
app.get( '/subscription/:id', routes.getSubscription);
app.post('/update',           routes.updateSubscription);
