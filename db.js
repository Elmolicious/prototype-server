var mongoose = require('mongoose');
var cradle = require('cradle');
//var nano = require('nano')('http://localhost:5984');

var couchdb =  new(cradle.Connection)('http://localhost:5984').database('subscriptions');

couchdb.exists(function(error, exists) {
    if (error)
        console.log('error occured while looking for CouchDatabase');
    else if (exists)
        console.log('databse exists');
    else {
        console.log('db does not exist, creating db');
        couchdb.create(function(error){
            console.log(error);
        });
    }
});

var mongoDb = mongoose.Connection;

var subscriptionSchema = new mongoose.Schema({
    customerName    : String,
    productName    : String,
    price : Number,
    id : Number
});
var Subscription = mongoose.model('Subscription',subscriptionSchema);

mongoose.connect('mongodb://localhost/subscriptions');
console.log('Connected to MongoDB');

exports.db = mongoDb;
exports.Subscription = Subscription;
exports.couchDb = couchdb;
