var db = require('./db');
var request = require('request');
var id = 1;

exports.setSubscription = function(req,res){
    new db.Subscription({
        customerName : req.body['customerName'],
        productName : req.body['productName'],
        price : req.body['price'],
        id : id++
    }).save(function(err) {
            if (err)
                return console.error(err);
        });


    /*db.couchDb.save('subscription',{
        customerName: req.body['customerName'],
        productName: req.body['productName'],
        price: req.body['price'],
        id: id++},function(error, res){
        if(error)
            console.log(error);
        else
            console.log(res);
    });*/

    notify({customerName : req.body.customerName,
            productName : req.body.productName,
            price : req.body.price}, 'Created Subscription');

    res.writeHead(201);
    res.write(JSON.stringify({id : (id-1)}));
    res.end();

};

exports.getSubscription = function(req,res){
    var data = {};
   db.Subscription.findOne({'id' : ''+req.params.id},'customerName productName price id', function(err, subscription){
       if(err)
          return console.log(err);
       if(!subscription)
          console.log('empty record/not found');

   data.customerName = subscription.customerName;
   data.productName = subscription.productName;
   data.price = subscription.price;
   data.id = req.params.id;

   res.writeHead(201);
   res.write(JSON.stringify(data));
   res.end();
    });
};

exports.updateSubscription = function(req,res){

    db.Subscription.findOneAndUpdate({'id' : ''+req.body.id},
        {'customerName' : req.body.customerName,
         'productName'  : req.body.productName,
         'price'        : req.body.price},
        function(err, subscription){
        if(err)
            return console.log(err);

            var data = {'customerName' : subscription.customerName,
                        'productName' : subscription.productName,
                        'price' : subscription.price,
                        'id' : req.body.id};

            notify(data,'Updated Subscription');
            res.writeHead(201);
            res.write(JSON.stringify(data));
            res.end();
    });
};

notify = function (data,action) {
    data.action = action;
    request.post({
            url : 'http://httpbin.org/post',
            body : JSON.stringify(data)},
        function(){
            console.log('Send Norification');
        });
};





